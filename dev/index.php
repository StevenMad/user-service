<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$app = new \Slim\App;
$app->get('/user', function (Request $request, Response $response) {
    return '[{"id":"12345","first_name":"Anthony","last_name":"Hopkins","email":"anthony-hopkins@email.com","created_date","2017-07-19T19:12:00"}]';
});

$app->get('/user/12345', function (Request $request, Response $response) {
    return '{"id":"12345","first_name":"Anthony","last_name":"Hopkins","email":"anthony-hopkins@email.com","created_date","2017-07-19T19:12:00"}';
});

$app->run();