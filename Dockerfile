FROM alpine

#add php7
RUN apk update && apk add php7 && apk add php7-json && apk add php7-phar
RUN apk add php7-mbstring php7-openssl php7-zlib openssl php7-simplexml php7-tokenizer php7-xmlwriter php7-dom

#add curl
RUN apk add curl
#add wget
RUN apk add wget

#add composer
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/bin/composer
#add phpunit for unit testing
RUN wget https://phar.phpunit.de/phpunit.phar | php && chmod +x phpunit.phar && mv phpunit.phar /usr/bin/phpunit 
#working directory
WORKDIR /app

ADD ./dev /app


EXPOSE 3000